REPOSITORIO MAVEN MOVION
========================

Repositorio para librerías usadas en movion


Inclusión de librerías propias en nuestro Maven
-----------------------------------------------

### Maven público. Incluir en el archivo build.gradle de la librería la siguiente linea de configuración:

```
apply from: 'https://bitbucket.org/movion/maven_repository/raw/releases/publish-bitbucket.gradle'
```

### Maven privado. Descargar el archivo build.gradle de la url 'https://bitbucket.org/movion/maven_repository/raw/releases/publish-bitbucket.gradle' e incluirlo en la raiz del proyecto y añadir esta linea en el archivo build.gradle

```
apply from: './publish-bitbucket.gradle'
```

### Incluir un archivo gradle.properties en la carpeta del proyecto de la librería con estas variables de configuración (no subir al repositorio el nombre de usuario ni la clave de bitbucket):
```
USERNAME=<usuario_bitbucket>
PASSWORD=<password_bitbucket>

ARTIFACT_VERSION=<version_libreria>
ARTIFACT_NAME=<nombre_proyecto_libreria>
ARTIFACT_PACKAGE=<nombre_paquete_libreria>
ARTIFACT_PACKAGING=aar

COMPANY=movion
REPOSITORY_NAME=maven_repository
```

### Para poder hacer despliegue de la librería en maven es necesario tener configuradas las claves ssh en la maquina local y configuradas en el usuario de bitbucket: https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html

### Para desplegar una nueva versión de la librería basta con ejecutar el siguiente comando por consola:
```
./gradlew --info uploadArchives
```



Uso en aplicaciones de una librería desplegada en nuestro Maven
---------------------------------------------------------------

### Maven público. Añadir la siguiente linea configuración en el archivo build.gradle del proyecto

```
allprojects {
    repositories {
        jcenter()
        maven {            
            url "https://api.bitbucket.org/1.0/repositories/movion/maven_repository/raw/releases"
        }
    }
}
```

### Maven privado. Añadir la siguiente linea configuración en el archivo build.gradle del proyecto con las credenciales

```
allprojects {
    repositories {
        jcenter()
        maven {
        	credentials {
                username USERNAME
                password PASSWORD
            }            
            url "https://api.bitbucket.org/1.0/repositories/movion/maven_repository/raw/releases"
        }
    }
}
```

Incluir un archivo gradle.properties en la carpeta del proyecto de la librería con estas variables de configuración (no subir al repositorio el nombre de usuario ni la clave de bitbucket):

```
USERNAME=<usuario_bitbucket>
PASSWORD=<password_bitbucket>
```

### Incluir la siguiente dependencia en el archivo de build.gradle de la aplicación:
```
dependencies {
    ...
    compile '<nombre_paquete_libreria>:<nombre_proyecto_libreria>:<version_libreria>'
    ...
}
```
